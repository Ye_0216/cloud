/**
--------------清理工作--------------
1. 把 app.json 里面的pages/logs/logs 删除了  顺便把 logs 页面也给删除了
2. 把 index 页面 里面的 index.js / .wxml / .wxss 里面的内容也清除掉了
3. app.js , onLuanch() 里面也清除了
--------------准备工作-------------------
1. 配置tabBar  创建 index 和  cart页面
  - 导入 assets 引入两种图标

2. 导入数据
  2.1 创建商品集合 goods  
  2.2 从数据文件夹里面导入 `goods-支付.json` 里面的数据

-------------展示首页列表数据-----------------
1. 获取数据
   - 获取全部数据 17条
   - 把 17条数据保存到 goods 
2. wxml 结构 (拷贝)
3. wxss 样式 (拷贝)

--------------上拉刷新---------------
1. 先限制 请求的条数 为 5条  limit(5)
2. 钩子 onReachBottom()
3. const LIMIT = 5
4. 分页
   let { _page, goods } = this.data

   let res = await goods_col.limit(LIMIT).skip( _page * LIMIT).get()

   this.setData({
      goods : [...goods,...res.data],
     _page : ++_page
   })   

--------------没有更多数据可加载的情况 (节流)-------------- 
1. 初始值  hasMore : true  // 判断是否有更多数据
2. 请求数据成功之后, 就知道有没有更多数据
   this.setData({
     hasMore : res.data.length === LIMIT
   })
3. 节流
  if(!this.data.hasMore) {
    return 
  }   

--------------加载框和提示框的封装-----------------------
 >> 参考 utils/asyncWx.js 

--------------下拉刷新-----------------------
1. 开启配置
     "enablePullDownRefresh": true,
     "backgroundTextStyle": "dark"
2. onPullDownRefresh()
  2.1 重置
  2.2 再次调用 加载数据
3. 手动停止下拉刷新 wx.stopPullDownRefresh()  


-----------------  轮播图数据   -------------------------
1. 加载轮播图数据
2.  需求 : 请求访问量前三的商品  count
    限制  :3个
    排序 : 降序
3. 代码 :
    let res = await goods_col.orderBy('count','desc').limit(3).get()


-----------------   跳转到详情页  -------------------------
1. 在商品外面套一个 navigator 导航组件 url='/pages/detail/detail?id={{item._id}}'
2. 在 detail.js 里面 根据 id 获取商品详情信息
3. 获取该id的商品信息 goods_col.doc(id).get()
4. 累加访问量


-----------------    点击下单   -------------------------
>> 把该商品加入到购物车里面
- 注册点击事件  catchtap : 不允许冒泡

- 加入购物车逻辑
1. 拿到点击要添加入到购物车的`商品`
2. 判断该商品在不在购物车里面
3. 如果不在, 把该商品添加到购物车里面, 并且新加一个字段 num = 1 
4. 如果在,  修改改商品的num值 累加

-----------------  设置 tab 右上角数字   -------------------------
1. wx.setTabbarBadge({
    index : 1,
    text  :  '' 
})

2. 点击 购物车 tabItem的时候, 触发的钩子,
// 点击 当前页面对应的tab
  onTabItemTap(){

    wx.setTabBarBadge({
      index: 1,
      text: '',
    })
  }
-----------------  进入到购物车   -----------------------
1. 添加自定义编译模式 刷新自动切换到购物车页面
2. 在购物车里面加载购物车 数据 展示 
3. 拷贝 wxml + wxss
4. 设置一下总价格 和总数量
5. 点击+1 累加数据
   累加完之后更新数据

-----------------  点击支付   -------------------------
>> 准备工作
   - appId
   - 商户号
   - 秘钥

>> 具体步骤   
  1. 发起订单 => 获取订单号 
  2. 预支付 => 获取支付所需要的5个参数
  3. 支付   => wx.requestPayment(..)
  4. 更新支付状态 => 未支付 => 已支付
  5. 清空购物车 (清空 carts 集合 里面的数据)

 */