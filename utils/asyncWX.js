/**
 * 暴露一个返回promise的提示框
 */
export const ms_Login = () => {
  return new Promise(resolve => {
    wx.showLoading({
      title: 'loading...',
      success: resolve
    })
  })
}

export const mh_Login = () => {
  return new Promise(resolve => {
    wx.hideLoading({
      success: resolve,
    })
  })
}

export const ms_model = (title,none) => {
  return new Promise(resolve => {
    wx.showToast({
      title,
      icon:  none === undefined ? 'none' : 'success',
      success: resolve
    })
  })
}