const db = wx.cloud.database()
const goods_col = db.collection('goods')
const carts_col = db.collection('carts')



import {
  mh_Login,
  ms_Login,
  ms_model
} from '../../utils/asyncWX'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //数据
    goods: [],
    //页码
    pageNum: 0,
    //标识是否已无数据,false为数据到底了
    flag: true,
    //轮播图数据
    swipers: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setTabBar()
    this.loadSwiperData()
    this.loadGoodData()
  },

  //获取数据
  async loadGoodData() {
    let {
      goods,
      pageNum
    } = this.data
    const LIMIT = 5

    //请求提示框
    await ms_Login()

    //限制请求条数，和开始的位置
    let res = await goods_col.limit(LIMIT).skip(pageNum * LIMIT).get()

    //关闭下拉刷新
    wx.stopPullDownRefresh()
    //关闭提示框
    await mh_Login()
    console.log(res.data)
    this.setData({
      //es6语法，将获取到新的数据和原数据合并
      goods: [...goods, ...res.data],
      pageNum: ++pageNum,
      flag: res.data.length === 5
    })

  },

  //获取访问量前三的good，将其图片作为轮播图图片源
  async loadSwiperData() {
    let res = await goods_col.orderBy('count', 'desc').limit(3).get()
    this.setData({
      swipers: res.data
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    //清除已操作的数据，并获取初始化数据
    this.setData({
      goods: [],
      pageNum: 0,
      flag: true
    })
    this.loadGoodData()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: async function () {
    if (!this.data.flag) {
      await ms_model('暂无更多数据')
      return
    }
    this.loadGoodData()
  },


  /**
   * 添加到购物车
   */
  async addCart(e) {
    //获取到点击传入的对象
    let {
      item
    } = e.currentTarget.dataset

    try {
      //数据库中已有这个商品
      await carts_col.doc(item._id).get()
      await carts_col.doc(item._id).update({
        data:{
          num : db.command.inc(1)
        }
      })
    } catch (error) {
      //数据库中没有这个商品
       await carts_col.add({
        data: {
          _id: item._id,
          price: item.price,
          num: item.count,
          imageSrc: item.imageSrc,
          title: item.title
        }
      })
    }
    this.setTabBar()
    await ms_model('下单成功','icon')

  },

  /**
   * 显示tabBar购物车数量 
   */
  async setTabBar(){
    let total = 0
    const res = await carts_col.get()
    res.data.forEach(item =>{
      total += item.num
    })

    if ( !total ) return

    wx.setTabBarBadge({
      index: 1,
      text: total+''
    })
  },


})