// pages/detail/detail.js
const db = wx.cloud.database()
const goods_col = db.collection('goods')

Page({
  /**
   * 页面的初始数据
   */
  data: {
    detail: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.loadDetailData(options.id)
  },

  //根据id获取详细信息,并将访问量+1
  async loadDetailData(id) {
    //拿到数据
    let ins = await goods_col.doc(id)

    //访问量+1
    await ins.update({
      data: {
        //字段值+1
        count: db.command.inc(1)
      }
    })

    let res = await ins.get()
    //赋值
    this.setData({
      detail: res.data
    })

  },



  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})