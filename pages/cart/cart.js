// pages/cart/cart.js
const db = wx.cloud.database()
const carts_col = db.collection('carts')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    carts: [],
    totalPrice: 0,
    totalCount: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.loadGetCarts()
  },

  /**
   * 获取购物车所有数据 
   */
  async loadGetCarts() {
    let res = await carts_col.get()
    this.setData({
      carts: res.data
    })
    this.setTotalCarts(res.data)
  },


  /**
   * 计算总价格和总数量
   */
  setTotalCarts(carts) {
    let totalPrice = 0
    let totalCount = 0
    carts.forEach(item => {
      totalPrice += item.price * item.num
      totalCount += item.num
    })
    this.setData({
      totalPrice,
      totalCount
    })

  },

  /**
   * 累加某一项商品
   */
  async addCount(e) {
    let {
      id
    } = e.currentTarget.dataset
    //更新数据库中对应字段值+1
    await carts_col.doc(id).update({
      data: {
        num: db.command.inc(1)
      }
    })
    //重新获取数据
    // this.loadGetCarts()
    let newsCarts = this.data.carts
    let goods = newsCarts.find(item => item._id == id)
    goods.num++
    //更新累加后的数据
    this.setData({
      carts: newsCarts
    })
    //重新计算价格数量
    this.setTotalCarts(newsCarts)


  },

  /**
   * 当点击的页面是tabBar页面触发
   */
  onTabItemTap() {
    //清除购物车显示数
    wx.setTabBarBadge({
      index: 1,
      text: ''
    })
  },
  // 开始支付
  async startpay() {

    //1. 发起订单 => 获取订单号  => 未支付
    let res1 = await wx.cloud.callFunction({
      name: 'makeOrder',
      data: {
        carts: this.data.carts
      }
    })
    // await ml_showToastSuccess('发起订单成功')
    const {
      order_number
    } = res1.result
    console.log('发起订单', order_number)

    //2. 预支付  => 获取支付所需要的5个参数
    let res2 = await wx.cloud.callFunction({
      name: 'pay',
      data: {
        order_number
      }
    })
    console.log('预支付', res2)

    //3. 发起支付
    // await ml_payment(res2.result)

    // await ml_showToastSuccess('支付成功')

    //4. 更新支付状态  => 已支付 
    let res3 = await wx.cloud.callFunction({
      name: 'updateStatus',
      data: {
        order_number
      }
    })
    console.log('更新状态', res3)

    //5. 清空购物车
    let res4 = await wx.cloud.callFunction({
      name: 'clearCarts'
    })
    console.log('清空购物车', res4)
    this.setData({
      carts: [],
      totalCount: 0,
      totalPrice: 0
    })

    // 跳转到订单页面
    // wx.navigateTo({
    //   url: '/pages/orders/orders',
    // })

  }
})